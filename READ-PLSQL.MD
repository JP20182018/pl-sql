create or replace PACKAGE BODY PKG_VENTA_DIGITAL_CARGA AS
 
/* Net Controlador: Carga/Obtener */
  PROCEDURE SP_OBTENER_CARGA(IN_ISAPRE IN VARCHAR2,
                                      N_RUT_TITULAR   IN  NUMBER,
                                       ORA_CURSOR            OUT NEW_CURSOR,
                                       /* Datos nuevos no registrados en API.xls */
                                       IN_IDAFILIADO    IN     NUMBER
                                       )
   IS
   
   BEGIN
      OPEN ORA_CURSOR FOR
      SELECT  CA.NOMBRES,
              CA.APELLIDO_PATERNO,
              CA.APELLIDO_MATERNO,
              CA.FECHA_NACIMIENTO,
              CA.SEXO,
              CA.EMAIL,
              DA.calle,
              DA.numero,
              DA.depto,
              DA.block,
              DA.villa,
              DA.LOCALIDAD,
              DA.region,
              'Santiago'     AS "CIUDAD" /*DA.ciudad, falta acceso a la DB*/,
              CA.fono_movil,
              'C'     AS "TipoBeneficiario"
FROM CARGAS CA
INNER JOIN AFILIADOS A ON  A.ID = CA.ID_AFILIADO
INNER JOIN DATOS_CONTACTO DA on A.ID = DA.ID_AFILIADO
  /* WHERE ROWNUM < 1001; */
WHERE /*A.RUT = N_RUT_TITULAR*/  A.ID = IN_IDAFILIADO AND A.ISAP_CEMPRESA = IN_ISAPRE;
END SP_OBTENER_CARGA;
